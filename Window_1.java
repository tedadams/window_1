package englishgame;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridBagConstraints;

import javax.swing.JFrame;
import javax.swing.WindowConstants;
import javax.xml.crypto.KeySelectorResult;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.JTable;
import java.util.LinkedList;

public class Window_1 extends JFrame {

	private static final String[] arraylist1 = null;

	public static void main(String args[]) {
		new Window_1();

	}

	// little english game by tedadams 2015
	
	
	// JFrame tutorial by youtube user: Derek Banas
	
	public Object button1;

	public Window_1() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// pop up-akna suurus
		this.setSize(500, 500);

		Toolkit tk = Toolkit.getDefaultToolkit();
		Dimension dim = tk.getScreenSize();
		// pikkust ja laiust arvestades jagab akna pooleks
		int xPos = (dim.width / 2) - (this.getWidth() / 2);
		int yPos = (dim.height / 2) - (this.getHeight() / 2);
		// ja paigutab akna ekraani keskele
		this.setLocation(xPos, yPos);
		// akna suurust ei saa muuta
		this.setResizable(false);
		// sulgeb akna vajutades punasele x-nupust
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// grammatikam�ngu pealkiri on Learn Grammar
		this.setTitle("Learn Grammar");

		JPanel thePanel = new JPanel();

		JLabel label1 = new JLabel("translation to estonian");
		// �lesande kirjeldus ingl k
		label1.setText("Put the words in the correct order.          Help?");
		// �lesande l�hikirjeldus, kui ei saadud �lesandest aru
		label1.setToolTipText(
				"Pane s�nad �igesse j�rjekorda. Vastused kirjuta allolevasse kasti. T�islause algab suure algust�hega ja l�peb punktiga. Vihje: Tekstit��tlusfailides teeb lause t�ielikuks taane ehk SPACE");

		thePanel.add(label1);

		// loon tekstiala
		JTextArea textarea1 = new JTextArea(10, 40);
		{

			// lisan tekstiala v�ljale
			thePanel.add(textarea1);
			// loon nupu. nupu peal on kirje "submit" hiirega peale liikudes
			// avaneb
			// abiteade
			JButton button1 = new JButton("Submit");
			button1.setEnabled(false);
			button1.setPreferredSize(new Dimension(200, 100));

			// see on nupu "submit" abiteade
			button1.setToolTipText(
					"Kui arvad, et oled s�nad �igesse j�rjekorda pannud, siis v�id klikata nupule 'Submit', et saata oma vastus anal��simiseks.");
			// ListenForButton button1 = new ListenForButton();
			// lisan v�ljale nupu
			thePanel.add(button1);
			// first sentence: "All the faith he had had had had no effect on
			// the
			// outcome of his life."
			textarea1.addKeyListener(new KeyListener() {
				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub

				}

				@Override
				public void keyTyped(KeyEvent e) {
					if (textarea1.getText()
							.equals("All the faith he had had had had no effect on the outcome of his life.")) {
						button1.setEnabled(true);

					} else {
						button1.setEnabled(false);
					}
				}

				@Override
				public void keyPressed(KeyEvent arg0) {

				}

			});

			button1.addMouseListener(new MouseListener() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if (button1.isEnabled()) {
						Window_2 nw = new Window_2();
						nw.NewScreen();

					}

				}

				@Override
				public void mousePressed(MouseEvent e) {

				}

				@Override
				public void mouseReleased(MouseEvent e) {

				}

				@Override
				public void mouseEntered(MouseEvent e) {
					// TODO Auto-generated method stub

				}

				@Override
				public void mouseExited(MouseEvent e) {
					// TODO Auto-generated method stub

				}
			});

		}

		JLabel label2 = new JLabel("sentence one1");

		label2.setText("he, the outcome, All, life, no, of, on, had, had, had, faith, effect, had, his, All, the");

		thePanel.add(label2);

		int ButtonClicked;

		this.add(thePanel);

		this.setVisible(true);
	}
}

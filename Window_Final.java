package englishgame;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Window_Final extends JFrame {

	private JPanel contentPane;
	private JFrame frmclass;

	/**
	 * Launch the application.
	 */
	public static void NewScreen() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window_Final frame = new Window_Final();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		new Window_Final();
	}

	/**
	 * Create the frame.
	 */

	public Object button2;

	public Window_Final() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setSize(500, 500);

		Toolkit tk = Toolkit.getDefaultToolkit();
		Dimension dim = tk.getScreenSize();

		int xPos = (dim.width / 2) - (this.getWidth() / 2);
		int yPos = (dim.height / 2) - (this.getHeight() / 2);

		this.setLocation(xPos, yPos);

		this.setResizable(false);

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setTitle("Okay!");

		JPanel thePanel = new JPanel();

		JLabel labelfinal = new JLabel("congrats");

		labelfinal.setText("Well Done!");

		thePanel.add(labelfinal);
		
		this.add(thePanel);
		
		this.setVisible(true);
	}

}

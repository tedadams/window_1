package englishgame;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

public class Window_4 extends JFrame {
	

		private JPanel contentPane;
		private JFrame frmclass;

		/**
		 * Launch the application.
		 */
		public static void NewScreen() {
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						Window_4 frame = new Window_4();
						frame.setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			new Window_4();
		}

		/**
		 * Create the frame.
		 */

		public Object button2;

		public Window_4() {
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setBounds(100, 100, 450, 300);
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			contentPane.setLayout(new BorderLayout(0, 0));
			setContentPane(contentPane);

			// pop up-akna suurus
			this.setSize(500, 500);

			Toolkit tk = Toolkit.getDefaultToolkit();
			Dimension dim = tk.getScreenSize();
			// pikkust ja laiust arvestades jagab akna pooleks
			int xPos = (dim.width / 2) - (this.getWidth() / 2);
			int yPos = (dim.height / 2) - (this.getHeight() / 2);
			// ja paigutab akna ekraani keskele
			this.setLocation(xPos, yPos);
			// akna suurust ei saa muuta
			this.setResizable(false);
			// sulgeb akna vajutades punasele x-nupust
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			// grammatikam�ngu pealkiri on Learn Grammar
			this.setTitle("Learn Grammar PART 4");

			JPanel thePanel = new JPanel();

			JLabel label1 = new JLabel("translation to estonian1");
			// �lesande kirjeldus ingl k
			label1.setText("Put the words in the correct order.          Help?");
			// �lesande l�hikirjeldus, kui ei saadud �lesandest aru
			label1.setToolTipText(
					"Pane s�nad �igesse j�rjekorda. Vastused kirjuta allolevasse kasti. T�islause algab suure algust�hega ja l�peb punktiga. Vihje: Tekstit��tlusfailides teeb lause t�ielikuks taane ehk SPACE");

			thePanel.add(label1);

			// loon tekstiala
			JTextArea textarea1 = new JTextArea(10, 40);
			// lisan tekstiala v�ljale
			thePanel.add(textarea1);
			// loon nupu. nupu peal on kirje "submit" hiirega peale liikudes avaneb
			// abiteade
			JButton button1 = new JButton("Submit");
			button1.setEnabled(false);
			button1.setPreferredSize(new Dimension(200, 100));

			// see on nupu "submit" abiteade
			button1.setToolTipText(
					"Kui arvad, et oled s�nad �igesse j�rjekorda pannud, siis v�id klikata nupule 'Submit', et saata oma vastus anal��simiseks.");
			
			// lisan v�ljale nupu
			thePanel.add(button1);
			// Second Sentence "The horse raced past the barn fell."
			// outcome of his life."
			
			textarea1.addKeyListener(new KeyListener() {
				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub

				}

				@Override
				public void keyTyped(KeyEvent e) {
					if (textarea1.getText()
							.equals("Time flies like an arrow, but fruit flies like a banana.")) {
						button1.setEnabled(true);

					} else {
						button1.setEnabled(false);
					}
				}

				@Override
				public void keyPressed(KeyEvent arg0) {

				}

			});

			button1.addMouseListener(new MouseListener() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if (button1.isEnabled()) {
						Window_5 nw = new Window_5();
						nw.NewScreen();
						
					} 
					
				} 
				

				@Override
				public void mousePressed(MouseEvent e) {
					

				}

				@Override
				public void mouseReleased(MouseEvent e) {
					

				}

				@Override
				public void mouseEntered(MouseEvent e) {
					// TODO Auto-generated method stub

				}

				@Override
				public void mouseExited(MouseEvent e) {
					// TODO Auto-generated method stub

				}
			});

				

			JLabel label2 = new JLabel("sentence four");

			label2.setText("banana, ,but, an, like, a, Time, flies, arrow, flies, like, fruit");

			thePanel.add(label2);

			int ButtonClicked;

			this.add(thePanel);

			this.setVisible(true);

		}

}
